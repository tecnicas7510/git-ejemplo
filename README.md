Tihs is a collection of classical example functions. They mostly don't work!

# Fibonacci
The Fibonacci sequence is a sequence in which each number is the sum of the two preceding ones.
Our sequence starts with F_0 = 1, F_1 = 1, and continues from there (F_2 = 2, F_3 = 3, etc)

# FizzBuzz
Count from 1 to some number, saying "Fizz" if the number is divisible by 3, "Buzz" if it's divisible
by 5, and "Fizzbuzz!" if it's divisible by both 3 and 5. To save time, we skip every other number