package ar.uba.fi.tdd;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FizzBuzzTest {
    @Test
    void fizzBuzzTo15() {
        assertEquals(
                List.of("Fizz", "Buzz", "Fizz", "Fizz", "Buzz", "Fizz", "Fizzbuzz!"),
                new FizzBuzz().apply(15)
        );
    }
}
