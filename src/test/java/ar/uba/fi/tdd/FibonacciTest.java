package ar.uba.fi.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FibonacciTest {
    @Test
    void fibonacciOf0() {
        assertEquals(
                1,
                new Fibonacci().apply(0)
        );
    }

    @Test
    void fibonacciOf1() {
        assertEquals(
                1,
                new Fibonacci().apply(1)
        );
    }

    @Test
    void fibonacciOf2() {
        assertEquals(
                2,
                new Fibonacci().apply(2)
        );
    }

    @Test
    void fibonacciOf3() {
        assertEquals(
                3,
                new Fibonacci().apply(3)
        );
    }

    @Test
    void fibonacciOf4() {
        assertEquals(
                5,
                new Fibonacci().apply(4)
        );
    }
}
