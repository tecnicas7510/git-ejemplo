package ar.uba.fi.tdd;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FizzBuzz implements Function<Integer, List<String>> {

    @Override
    public List<String> apply(Integer value) {
        return IntStream.rangeClosed(0, value)
                .mapToObj(FizzBuzz::convert)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private static String convert(int number) {
        if (isDivisible(number, 6)) {
            return "Fizzbuzz!";
        } else if (isDivisible(number, 2)) {
            return "Buzz";
        } else if (isDivisible(number, 3)) {
            return "Fizz";
        } else {
            return null;
        }
    }

    private static boolean isDivisible(int number, int divisor) {
        return number % divisor == 0;
    }
}