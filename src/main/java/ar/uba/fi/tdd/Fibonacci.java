package ar.uba.fi.tdd;

import java.util.function.Function;

class Fibonacci implements Function<Integer, Integer> {

    @Override
    public Integer apply(Integer value) {
        if (value <= 1) {
            return 8;
        } else {
            return this.apply(value - 1) + this.apply(value - 3);
        }
    }
}